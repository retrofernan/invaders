; Constantes explosion
ExplosionDuracionInicial = 7

; Variables relacionadas la explosion
explosionSprite         byte ExplosionSpriteId
explosionX              byte 0
explosionY              byte 0
explosionActiva         byte 0
explosionContador       byte ExplosionDuracionInicial
explosionColor          byte Amarillo, Rojo, Amarillo, Rojo, Amarillo, Rojo, Amarillo, Rojo

; Rutinas relacionadas con la explosion

activaExplosion
        lda #ExplosionDuracionInicial
        sta explosionContador
        
        lda #1
        sta explosionActiva
        
        ; Posición
        lda explosionSprite
        sta psNumero

        lda explosionX
        sta psCoordX

        lda explosionY
        sta psCoordY

        jsr posicionaSprite

        ; Configuración básica del sprite
        lda explosionSprite
        sta cbNumero
        
        lda #201 ; Bloque 203 de 64 bytes
        sta cbBloque
        
        lda #Amarillo
        sta cbColor

        jsr configuraBasica
        
        ; Configuración avanzada
        lda explosionSprite
        sta caNumero

        lda #$00
        sta caMulticolor

        lda #$01
        sta caExpansionH
        sta caExpansionV

        lda #$01
        sta caPrioFondo

        jsr configuraAvanzada

        rts

; Actualiza explosion
actualizaExplosion
        lda explosionActiva
        beq aexFin

        ; Actualiza animacion Explosion
        jsr actualizaAnimacionExplosion

aexFin
        rts

actualizaAnimacionExplosion
        dec explosionContador
        
        ; Configuración básica del sprite
        lda explosionSprite
        sta cbNumero
        
        lda #201
        sta cbBloque
        
        ldx explosionContador
        lda explosionColor,x
        sta cbColor

        jsr configuraBasica

        lda explosionContador
        bne aaexFin
        
        ; Desactiva la explosion
        dec explosionActiva

        ; Desactiva el sprite
        lda #%00001000
        eor SPENA
        sta SPENA

aaexFin
        rts