; Variables relacionadas con el disparo
disparoSprite           byte DisparoSpriteId
disparoX                byte JugadorXInicial
disparoY                byte JugadorYInicial
disparoDisparado        byte $00

; Rutinas relacionadas con el disparo

inicializaDisparo

        ; Posición
        lda disparoSprite
        sta psNumero

        lda disparoX
        sta psCoordX

        lda disparoY
        sta psCoordY

        jsr posicionaSprite

        ; Configuración básica del sprite
        lda disparoSprite
        sta cbNumero
        
        lda #193 ; Bloque 193 de 64 bytes
        sta cbBloque
        
        lda #Blanco
        sta cbColor

        jsr configuraBasica

        ; Configuración avanzada
        lda disparoSprite
        sta caNumero

        lda #$00
        sta caMulticolor

        lda #$01
        sta caExpansionH
        sta caExpansionV

        lda #$01
        sta caPrioFondo

        jsr configuraAvanzada

        rts

; Actualiza el disparo

actualizaDisparo

        ;Actualiza la posición del disparo
        jsr actualizaPosicionDisparo

        ;Actualiza colision disparo
        jsr actualizaColisionDisparo

        rts

; Sub actualizaColisionDisparo
actualizaColisionDisparo
        lda SPSPCL
        and #%00000010

        beq acdFin

        ; si la colision es con el jugador, se ignora
        lda disparoY
        cmp jugadorY
        beq acdFin

        lda #0
        sta disparoDisparado

        lda jugadorX
        sta disparoX
        
        lda jugadorY
        sta disparoY
acdFin
        rts

actualizaPosicionDisparo
        lda #Disparo
        bit joy2
        
        bne adpComprobarDisparado

        lda #$01
        sta disparoDisparado

adpComprobarDisparado
        lda disparoDisparado

        bne apdDisparado

        ; sigue al jugador
        lda jugadorX
        sta disparoX
        
        jmp apdLimiteSuperior

apdDisparado
        ; disparado
        dec disparoY
        dec disparoY

apdLimiteSuperior
        lda disparoY
        ; bit DisparoLimiteSuperior

        bne apdActualiza
        
        lda #$00
        sta disparoDisparado

        lda jugadorX
        sta disparoX
        
        lda jugadorY
        sta disparoY

apdActualiza
        ; actualiza
        lda disparoSprite
        sta psNumero

        lda disparoX
        sta psCoordX

        lda disparoY
        sta psCoordY

        jsr posicionaSprite

        rts