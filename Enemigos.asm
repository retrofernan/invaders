; Constantes Enemigos
Enemigo1SpriteId = 2
EnemigoXInicial = 100
EnemigoYInicial = 60
EnemigoDireccionInicial = 0
EnemigoNumFrames = 2
EnemigoColor = AzulClaro

; Variables relacionadas con el disparo
enemigoSprite           byte Enemigo1SpriteId
enemigoVivo             byte 1 
enemigoX                byte EnemigoXInicial
enemigoY                byte EnemigoYInicial
enemigoDireccion        byte EnemigoDireccionInicial
enemigoRetardoInicial   byte 10
enemigoRetardo          byte EnemigoRetardoInicial
enemigoTablaFrames      byte 194, 195
enemigoFrame            byte 1

; Rutinas relacionadas con los enemigos

inicializaEnemigo

        ; Posición
        lda enemigoSprite
        sta psNumero

        lda enemigoX
        sta psCoordX

        lda enemigoY
        sta psCoordY

        jsr posicionaSprite

        ; Configuración básica del sprite
        lda enemigoSprite
        sta cbNumero
        
        lda enemigoTablaFrames ; Bloque 194 de 64 bytes
        sta cbBloque
        
        lda #EnemigoColor
        sta cbColor

        jsr configuraBasica

        ; Configuración avanzada
        lda enemigoSprite
        sta caNumero

        lda #$00
        sta caMulticolor

        lda #$01
        sta caExpansionH
        sta caExpansionV

        lda #$01
        sta caPrioFondo

        jsr configuraAvanzada

        rts

; Actualiza el enemigo
actualizaEnemigo
        lda enemigoVivo
        beq aeFin

        ;Actualiza la posición del disparo
        jsr actualizaPosicionEnemigo

        ;Actualiza colision Enemigo
        jsr actualizaColisionEnemigo

aeFin
        rts

; Sub aceleraEnemigo
aceleraEnemigo
        inc enemigoY
        inc enemigoY
        inc enemigoY
        inc enemigoY
        inc enemigoY
        inc enemigoY
        inc enemigoY
        inc enemigoY

        dec enemigoRetardoInicial
        dec enemigoRetardoInicial

        rts

; Sub actualizaColisionEnemigo 
actualizaColisionEnemigo
        lda SPSPCL
        and #%00000100

        beq aceFin

        ; Desactiva el enemigo
        dec enemigoVivo

        ; Desactiva el sprite
        lda #%00000100
        eor SPENA
        sta SPENA
        
        ; Activa Explosion
        lda enemigoX
        sta explosionX

        lda enemigoY
        sta explosionY

        jsr activaExplosion
aceFin
        rts

; Sub actualizaPosicionEnemigo
actualizaPosicionEnemigo
        dec enemigoRetardo
        ; lda enemigoRetardo

        bne apeActualiza

        ; reset retardo
        lda enemigoRetardoInicial
        sta enemigoRetardo

        ; mueve enemigo
        lda enemigoDireccion
        
        bne apeMueveIzquierda
        
        ; mueve derecha
        inc enemigoX
        inc enemigoX
        ;inc enemigoX
        ;inc enemigoX
        
        jmp apeLimiteIzquierdo

apeMueveIzquierda
        ; mueve izquierda
        dec enemigoX
        dec enemigoX
        ;dec enemigoX
        ;dec enemigoX

apeLimiteIzquierdo
        lda enemigoX
        cmp #JugadorLimiteIzquierdo-4

        bne apeLimiteDerecho
        
        lda #0
        sta enemigoDireccion
        
        jsr aceleraEnemigo

apeLimiteDerecho
        lda enemigoX
        cmp #JugadorLimiteDerecho+4

        bne apeFrame

        lda #1
        sta enemigoDireccion

        jsr aceleraEnemigo

apeFrame
        ; cambia frame
        dec enemigoFrame
        
        lda enemigoFrame
        cmp #255

        bne apeActualiza

        lda #EnemigoNumFrames - 1
        sta enemigoFrame
        
apeActualiza
        ; actualiza frame
        lda enemigoSprite
        sta cbNumero
        
        ldx enemigoFrame
        lda enemigoTablaFrames,x ; Bloque 194 de 64 bytes
        sta cbBloque
        
        lda #EnemigoColor
        sta cbColor

        jsr configuraBasica

        ; actualiza posicion
        lda enemigoSprite
        sta psNumero

        lda enemigoX
        sta psCoordX

        lda enemigoY
        sta psCoordY

        jsr posicionaSprite

        rts